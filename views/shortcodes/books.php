<div id="supadu-books-holder"></div>

<script id="supadu-book-template" type="text/x-handlebars-template">
  <div class="supadu-book">
    <img class="supadu-book__image" src="{{ image }}" >
    <h2 class="supadu-book__title">
      {{ title }}
    </h2>
  </div>
</script>