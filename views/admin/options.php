<div class="wrap">
  <h1><?= esc_html( get_admin_page_title() ); ?></h1>
  <form action="options.php" method="post">
    <?php
    // output security fields for the registered setting
    settings_fields('supadu_plugin_options');
    // output setting sections and their fields
    do_settings_sections('supadu_plugin_options');
    // output save settings button
    submit_button('Save Settings');
    ?>
  </form>
</div>