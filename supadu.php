<?php

/*
Plugin Name: Supadu Technical Test Plugin
Description: Plugin which interacts with supadu API and displays books on pages or posts.
Version:     1.0.0
Author:      Tameem Safi
Author URI:  https://safi.me.uk
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: supadu
*/

require_once __DIR__ . '/vendor/autoload.php';

if( class_exists( '\\SupaduPlugin\\Core\\Setup' ) ) {
  define( 'SUPADU_PLUGIN_FILE_PATH', __FILE__ );
  $GLOBALS['SupaduPlugin'] = \SupaduPlugin\Core\Setup::get_instance();
}