<?php

namespace SupaduPlugin\Helpers;

class CoreHelper {

  /*
   * @return string URL to plugin directory with trailing slash.
   */
  public static function get_plugin_directory_uri() {
    return plugin_dir_url( SUPADU_PLUGIN_FILE_PATH );
  }

  /*
   * @return string Path to plugin directory with trailing slash.
   */
   public static function get_plugin_directory_path() {
    return plugin_dir_path( SUPADU_PLUGIN_FILE_PATH );
  }

  /*
   * @param string path relative to current plugin directry without leading slash.
   * @return string full path relative to current plugin directory.
   */
  public static function get_plugin_file_uri( $relative_path ) {
    return self::get_plugin_directory_uri() . $relative_path;
  }

  /*
   * @param string path of view php file relative to view folder.
   * @return string full path of view php file including plugin path.
   */
  public static function get_view_template_path( $view_path ) {
    return self::get_plugin_directory_path() . 'views/' . $view_path;
  }

  /*
   * @return void returns the book information from API
   */
  public static function get_book_with_isbn( $books_api_url, $api_key, $ISBN ) {
    $res = self::get_api_response( $books_api_url . $ISBN, $api_key );
    // Check if status code is 200
    if( $res->getStatusCode() != 200 ) {
      return false;
    }
    
    // Add book to temporary array
    $resBody = json_decode( $res->getBody() );
    $book = reset( $resBody->data->book );
    return $book;
  }

  /*
   * @return void returns the book information from API
   */
   public static function get_books_with_collection_name( $service_url, $api_key, $collection_name ) {
    $res = self::get_api_response( $service_url . 'search?collection=' . $collection_name, $api_key );
    // Check if status code is 200
    if( $res->getStatusCode() != 200 ) {
      return false;
    }
    
    // Add book to temporary array
    $resBody = json_decode( $res->getBody() );
    $books = $resBody->data->search;
    return $books;
  }

  /*
   * @return void repsonse in guzzle object format.
   */
  public static function get_api_response( $url, $api_key ) {
    $client = new \GuzzleHttp\Client();
    // Get request to api for books
    $res = $client->request('GET', $url , [
      'headers' => array(
        'Accept'     => 'application/json',
        'Accept-Encoding' => 'gzip',
        'x-apikey' => $api_key
      )
    ]);
    return $res;
  }

}