<?php

namespace SupaduPlugin\Shortcodes;

use SupaduPlugin\Shortcodes\BooksISBN;

class Shortcodes {

  public function __construct() {

    // Setup books query with shortcode
    new Books();

  }

}