<?php

namespace SupaduPlugin\Shortcodes;

use SupaduPlugin\Helpers\CoreHelper;

class Books {

  public $shortcode_name = 'supadu-books';
  public $shortcode_script_tag = 'supadu-plugin-shortcode-script';
  public $handlebar_script_tag = 'supadu-plugin-handlebar';
  public $shortcode_style_tag = 'supadu-plugin-shortcode-style';

  public function __construct() {
    add_shortcode( $this->shortcode_name, array( $this, 'supadu_books_shortcode' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
    add_action( 'wp_ajax_get_books', array( $this, 'get_books' ) );
    add_action( 'wp_ajax_nopriv_get_books', array( $this, 'get_books' ) );
  }

  /*
   * Handle shortcode for displaying books using ISBN numbers.
   *
   * @param mixed attributes passed to shortcode.
   */
  function supadu_books_shortcode( $atts ) {

    // Filter attributes passed to shortcode
    $atts = shortcode_atts(
      array(
        'isbns' => false,
        'ajax' => false,
        'collection' => false
      ),
      $atts,
      $this->shortcode_name
    );

    // Check there if ISBNs have been passed
    if( empty( $atts['isbns'] ) && empty( $atts['collection'] ) ) {
      return __( 'No ISBNs or collection name provided.', 'supadu' );
    }

    if( !empty( $atts['ajax'] ) && ( strtolower($atts['ajax']) == 'true' || strtolower($atts['ajax']) == 'yes' ) ) {
      $atts['ajax'] = true;
    } else {
      $atts['ajax'] = false;
    }

    // Check to make sure service url and API key is enterd in settings
    $service_url = get_option( 'supadu_plugin_service_url' );
    $books_api_url = $service_url . 'book/';
    $api_key = get_option( 'supadu_plugin_api_key' );

    if( empty( $service_url ) || empty( $api_key ) ) {
      return __( 'Please enter service URL and API key.', 'supadu' );
    }
    
    // ISBN numbers
    if( empty( $atts['collection'] ) ) {

      $ISBN_Numbers = explode( ',', $atts['isbns'] );
      
      // Check if ISBN numbers are empty
      if( empty( $ISBN_Numbers ) ) {
        return __( 'No ISBNs provided.', 'supadu' );
      }
  
      // sanitize all isbn numbers
      $ISBN_Numbers = array_map( 'trim', $ISBN_Numbers );
  
      // Check if ajax should be used
      if( $atts['ajax'] == false ) {
  
        $client = new \GuzzleHttp\Client();
        $books_info = array();
  
        foreach( $ISBN_Numbers as $number ) {
          $book = CoreHelper::get_book_with_isbn( $books_api_url, $api_key, $number );
          if( !empty( $book ) ) {
            $books_info[] = $book;
          }
        }
  
        wp_localize_script(
          $this->shortcode_script_tag,
          'SUPADU_BOOKS_PLUGIN_DATA',
          array(
            'books' => $books_info,
            'ajax' => false,
          )
        );
  
        wp_enqueue_script( $this->handlebar_script_tag );
        wp_enqueue_script( $this->shortcode_script_tag );
        wp_enqueue_style( $this->shortcode_style_tag );
        
        return $this->show_shortcode_view();
  
      }
      
      wp_localize_script(
        $this->shortcode_script_tag,
        'SUPADU_BOOKS_PLUGIN_DATA',
        array(
          'ajax' => true,
          'ajax_url' => admin_url( 'admin-ajax.php' ),
          'ajax_nonce' => wp_create_nonce( 'supadu_get_book_isbn' ),
          'isbns' => $ISBN_Numbers,
        )
      );
  
      wp_enqueue_script( $this->handlebar_script_tag );
      wp_enqueue_script( $this->shortcode_script_tag );
      wp_enqueue_style( $this->shortcode_style_tag );
      
      return $this->show_shortcode_view();

    }

    // Collection name
    $collection_name = sanitize_text_field( $atts['collection'] );

    if( empty( $collection_name ) ) {
      return __( 'No collection name provided', 'supadu' );
    }

    if( empty( $atts['ajax'] ) ) {

      $books = CoreHelper::get_books_with_collection_name( $service_url, $api_key, $collection_name );
      
      wp_localize_script(
        $this->shortcode_script_tag,
        'SUPADU_BOOKS_PLUGIN_DATA',
        array(
          'books' => $books,
          'ajax' => false,
        )
      );
  
      wp_enqueue_script( $this->handlebar_script_tag );
      wp_enqueue_script( $this->shortcode_script_tag );
      wp_enqueue_style( $this->shortcode_style_tag );
      
      return $this->show_shortcode_view();

    }

    wp_localize_script(
      $this->shortcode_script_tag,
      'SUPADU_BOOKS_PLUGIN_DATA',
      array(
        'ajax' => true,
        'ajax_url' => admin_url( 'admin-ajax.php' ),
        'ajax_nonce' => wp_create_nonce( 'supadu_get_book_isbn' ),
        'collection' => $collection_name,
      )
    );

    wp_enqueue_script( $this->handlebar_script_tag );
    wp_enqueue_script( $this->shortcode_script_tag );
    wp_enqueue_style( $this->shortcode_style_tag );
    
    return $this->show_shortcode_view();

  }

  /*
   * @return void returns the html view for shortcode.
   */
  function show_shortcode_view() {
    ob_start();
    include_once CoreHelper::get_view_template_path( 'shortcodes/books.php' );
    return ob_get_clean();
  }

  /*
   * Enqueues the scripts required by the shortcode.
   */
  function enqueue_scripts() {
    // Register shortcode js for later use
    wp_register_script(
      $this->shortcode_script_tag,
      CoreHelper::get_plugin_file_uri( 'assets/js/shortcode-isbn.js' ),
      array(
        'jquery',
        $this->handlebar_script_tag
      )
    );

    wp_register_script(
      $this->handlebar_script_tag,
      CoreHelper::get_plugin_file_uri( 'assets/js/handlebar.js' ),
      array(
        'jquery'
      )
    );

    // Register shortcode css for later use
    wp_register_style(
      $this->shortcode_style_tag,
      CoreHelper::get_plugin_file_uri( 'assets/css/style.css' )
    );
  }

  /*
   * Grabs the book with ISBN from service API
   *
   * @return json return json output with error message or success message.
   */
  function get_books() {

    $nonce = sanitize_text_field( $_POST['nonce'] );

    if( !wp_verify_nonce( $nonce, 'supadu_get_book_isbn' ) ) {
      wp_send_json_error(array(
        'message' => __( 'Nonce is invalid.', 'supadu' )
      ));
    }

    $type = sanitize_text_field( $_POST['type'] );

    if( empty( $type ) ) {
      wp_send_json_error(array(
        'message' => __( 'Type is invalid.', 'supadu' )
      ));
    }

    // Check to make sure service url and API key is enterd in settings
    $service_url = get_option( 'supadu_plugin_service_url' );
    $books_api_url = $service_url . 'book/';
    $api_key = get_option( 'supadu_plugin_api_key' );

    if( empty( $service_url ) || empty( $api_key ) ) {
      wp_send_json_error(array(
        'message' => __( 'Service URL and API key not set.', 'supadu' )
      ));
    }

    if( $type == 'ISBN' ) {

      $ISBN = sanitize_text_field( $_POST['ISBN'] );
      
      if( empty( $ISBN ) ) {
        wp_send_json_error(array(
          'message' => __( 'ISBN is invalid.', 'supadu' )
        ));
      }
  
      $book = CoreHelper::get_book_with_isbn( $books_api_url, $api_key, $ISBN );
  
      if( empty( $book ) ) {
        wp_send_json_error(array(
          'message' => __( 'Book not found.', 'supadu' )
        ));
      }
  
      wp_send_json_success(array(
        'message' => $book
      ));

    }

    if( $type == 'COLLECTION' ) {

      $collection_name = sanitize_text_field( $_POST['collection'] );

      if( empty( $collection_name ) ) {
        wp_send_json_error(array(
          'message' => __( 'Collection name is invalid.', 'supadu' )
        ));
      }

      $books = CoreHelper::get_books_with_collection_name( $service_url, $api_key, $collection_name );

      if( empty( $books ) ) {
        wp_send_json_error(array(
          'message' => __( 'Books not found.', 'supadu' )
        ));
      }

      wp_send_json_success(array(
        'message' => $books
      ));

    }

  }

}