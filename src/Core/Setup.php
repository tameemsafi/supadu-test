<?php

namespace SupaduPlugin\Core;

use SupaduPlugin\Admin\Options;
use SupaduPlugin\Shortcodes\Shortcodes;

class Setup {
  
  private static $instance;

  public function __construct() {

    // Setup admin options
    new Options();

    // Init shortcodes
    new Shortcodes();

  }

  /*
   * @return \SupaduPlugin\Core\Setup returns a single instance of the class.
   */
  public static function get_instance() {
    if( self::$instance == NULL ) {
      self::$instance = new self();
    }
    return self::$instance;
  }

}