<?php

namespace SupaduPlugin\Admin;

use SupaduPlugin\Helpers\CoreHelper;

class Options {

  public function __construct() {
    add_action( 'admin_menu', array( $this, 'add_options_page' ) );
  }

  /*
   * Setup the admin options for the plugin
   */
  function add_options_page() {

    add_menu_page(
      'Supadu Plugin', // Page title
      'Supadu Options', // Menu title
      'manage_options', // Capability
      'supadu_plugin_options', // Menu slug
      array( $this, 'plugin_options_html' ), // Callback for page view
      'dashicons-admin-generic', // Icon url
      20 // Priority
    );

    $this->setup_settings();

  }

  /*
   * Sets up the plugin setting fields.
   */
  function setup_settings() {
    
    // Add new setting for supadu plugin options page
    register_setting( 'supadu_plugin_options', 'supadu_plugin_options' );

    // Add new section for supadu plugin options page
    add_settings_section(
      'supadu_plugin_options_section',
      __( 'Supadu API Settings', 'supadu' ),
      false,
      'supadu_plugin_options'
    );

    // Add new setting for API key
    register_setting(
      'supadu_plugin_options',
      'supadu_plugin_api_key',
      array(
        'type' => 'string',
        'sanitize_callback' => array( $this, 'sanitize_api_key' )
      )
    );

    // register api key field in "supadu_plugin_options_section" section on supadu options page
    add_settings_field(
      'supadu_plugin_api_key',
      __( 'API Key', 'supadu' ),
      array( $this, 'api_key_field_cb' ),
      'supadu_plugin_options',
      'supadu_plugin_options_section',
      array(
        'label_for' => 'supadu_plugin_api_key',
        'class' => 'supadu_plugin_api_key',
        'api_key' => get_option( 'supadu_plugin_api_key' ),
      )
    );

    // Add new setting for service URL
    register_setting(
      'supadu_plugin_options',
      'supadu_plugin_service_url',
      array(
        'type' => 'string',
        'sanitize_callback' => array( $this, 'sanitize_service_url' )
      )
    );

    // register service url field in "supadu_plugin_options_section" section on supadu options page
    add_settings_field(
      'supadu_plugin_service_url',
      __( 'Service URL', 'supadu' ),
      array( $this, 'service_url_field_cb' ),
      'supadu_plugin_options',
      'supadu_plugin_options_section',
      array(
        'label_for' => 'supadu_plugin_service_url',
        'class' => 'supadu_plugin_service_url',
        'url' => get_option( 'supadu_plugin_service_url' ),
      )
    );

  }

  /*
   * @return void returns the options page view html.
   */
  function plugin_options_html() {

    // check user capabilities
    if (!current_user_can('manage_options')) {
      return;
    }
 
    // check if the user have submitted the settings
    // wordpress will add the "settings-updated" $_GET parameter to the url
    if ( isset( $_GET['settings-updated'] ) && empty( get_settings_errors( 'supadu_plugin_messages') )) {
      add_settings_error( 'supadu_plugin_messages', 'supadu_plugin_message', __( 'Settings Saved', 'tsp' ), 'updated' );
    }
    
    // Display the messages
    settings_errors( 'supadu_plugin_messages' );

    include_once CoreHelper::get_view_template_path( 'admin/options.php' );

  }

  /*
   * @param mixed arguments passed when adding setting field.
   * @return void returns the api key field html.
   */
  function api_key_field_cb( $args ) {
    include_once CoreHelper::get_view_template_path( 'admin/fields/api-key.php' );
  }

  /*
   * @param mixed arguments passed when adding setting field.
   * @return void returns the service url field html.
   */
  function service_url_field_cb( $args ) {
    include_once CoreHelper::get_view_template_path( 'admin/fields/service-url.php' );
  }

  /*
   * Checks if the service url is valid
   *
   * @param mixed data from user input.
   * @return void returns data if valid and adds settings error if invalid.
   */
  function sanitize_service_url( $data ) {

    if( empty( $data ) ) {
      $this->add_settings_error_message( 'Please enter a Service URL' );
    } elseif ( filter_var( $data, FILTER_VALIDATE_URL ) === FALSE ) {
      $this->add_settings_error_message( 'Service URL is not valid' );
    } else {
      return $data;
    }

  }

  /*
   * Checks if the service url is valid
   *
   * @param mixed data from user input.
   * @return void returns data if valid and adds settings error if invalid.
   */
  function sanitize_api_key( $data ) {

    if( empty( $data ) ) {
      $this->add_settings_error_message( 'Please enter an API key' );
    } else {
      return $data;
    }

  }
  
  /*
   * Adds an error message using for the plugin settings page
   *
   * @param string 
   * @return void returns data if valid and adds settings error if invalid.
   */
  private function add_settings_error_message( $message ) {
    add_settings_error(
      'supadu_plugin_messages',
      esc_attr( 'settings_updated' ),
      __( $message , 'supadu' ),
      'error'
    );
  }

}