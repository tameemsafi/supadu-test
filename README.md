## Supadu Technical Test

### Setup
1. Install plugin and activate it using wordpress admin dashboard.
2. Go into the 'Supadu Options' page from the left navigation menu in the wordpress dashboard.
3. Enter your the service url and API key for supadu.

### List books using ISBN

You can list books inside any post/page with ISBN numbers using this shortcode.

```
[supadu-books isbns="9781509815494, 9781509800254, 9781509842186, 9781509842148, 9781509829941" ajax="true"]
```

**isbns**
This attribute requies a comma seperated string with the ISBN numbers.

**ajax**
This attribute requires a string with a value of 'true' to enable ajax and 'false' to disable ajax.

### List books using collection name

You can list books inside any post/page with the collection name using this shortcode.

```
[supadu-books collection="science-fiction-essentials" ajax="true"]
```

**collection**
This attribute requires a string which represents the name of the collection.

**ajax**
This attribute requires a string with a value of 'true' to enable ajax and 'false' to disable ajax.

### Note
- All ajax requests are proxied through wordpress ajax method to prevent leaking the API keys