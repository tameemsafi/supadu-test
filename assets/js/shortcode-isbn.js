jQuery(document).ready(function($) {

  var bookTemplate = $( '#supadu-book-template' ).html();
  var booksHolder = $( '#supadu-books-holder' );
  var template = Handlebars.compile( bookTemplate );

  var data = window.SUPADU_BOOKS_PLUGIN_DATA;

  if( data.ajax && data.isbns ) {
    for(var i = 0; i < data.isbns.length; i++) {
      var number = data.isbns[i];
      $.ajax({
        method: 'POST',
        url: data.ajax_url,
        data: {
          action: 'get_books',
          type: 'ISBN',
          ISBN: number,
          nonce: data.ajax_nonce,
        },
        dataType: 'json',
        accepts: 'application/json',
        cache: false,
        success: function( data, status, xhr ) {
          if( data.success ) {
            appendBook( data.data.message );
          }
        }
      });
    }
  } if( data.ajax && data.collection ) {
    $.ajax({
      method: 'POST',
      url: data.ajax_url,
      data: {
        action: 'get_books',
        type: 'COLLECTION',
        collection: data.collection,
        nonce: data.ajax_nonce,
      },
      dataType: 'json',
      accepts: 'application/json',
      cache: false,
      success: function( data, status, xhr ) {
        if( data.success ) {
          for( var i = 0; i < data.data.message.length; i++ ) {
            appendBook( data.data.message[i] );
          }
        }
      }
    });
  } else {
    for( var i = 0; i < data.books.length; i++ ) {
      appendBook( data.books[i] );
    }
  }

  function appendBook( book ) {
    var compiled = template( book );
    booksHolder.append( compiled );
  }

});